package com.xzjie.cms.wechat.handler;

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;

/**
 * @author vito
 * @since 2023/6/28 5:06 PM
 */
@Slf4j
public abstract class AbstractHandler implements WxMpMessageHandler {
}
